﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlickZone : MonoBehaviour
{
    private Image image;
    public Color color;
    [SerializeField] public Color defaultColor;
    private bool visible = false;
    private bool access;
    public float flickSpeed;
    [SerializeField] private UIController UICtrl;

    void Start()
    {
        image = GetComponent<Image>();
        color = defaultColor;
        image.alphaHitTestMinimumThreshold = 0.5f;
    }

    void Update()
    {
        if(access)
        {
            if (visible)
            {
                color.a -= Time.deltaTime * flickSpeed;
                if (color.a <= 0)
                {
                    visible = false;
                }
            }
            else
                color.a += Time.deltaTime * flickSpeed;
            image.color = color;
            if (color.a >= 0.4f)
            {
                visible = true;
            }
        }
    }

    public void AccessFlick()
    {
        if(!access) UICtrl.flickObjs++;
        UICtrl.CheckFlick();
        access = true;
    }

    public void StopFlick()
    {
        access = false;
        image.color = defaultColor;
    }
}

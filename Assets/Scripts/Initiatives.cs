﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Initiatives : MonoBehaviour
{
    private UIController UICtrl;
    private PlayerInfo playerInfo;
    [SerializeField] private Button button;
    [SerializeField] private Text buttonText;
    [SerializeField] private Text describe;
    [SerializeField] private Text nameText;
    [SerializeField] public Text coins;
    [SerializeField] private Image inflationFill;
    [SerializeField] private Image supportFill;
    public List<Addiction> listInitiatives;
    private string typeInitiative;
    public DataInitiatives data = new DataInitiatives();
    private string curInitiative;
    public Dictionary<string, bool> initiatives = new Dictionary<string, bool>();
    public float price;

    private void Start()
    {
        playerInfo = FindObjectOfType<PlayerInfo>();
        UICtrl = FindObjectOfType<UIController>();
        LoadData();
        UpdateButtons();
        UpdateSpecifications();
    }

    #region Народные Инициативы

    public void KeepHead()
    {
        typeInitiative = "Ore";
        if (!initiatives.ContainsKey("KeepHead")) initiatives.Add("KeepHead", false);
        if (initiatives["KeepHead"] == false)
        {
            curInitiative = "KeepHead";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if(playerInfo.coins<price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Береги голову";
        describe.text = "Построить в шахтах деревянные опоры, для обеспечения безопасности рабочих.\n" + "Повышает поддержку населения рудодобывающих поселений, снижает агрессию мятежников.\n\n" + "Цена инициативы: " + price;
    }

    public void Jewelery()
    {
        typeInitiative = "Ore";
        if (!initiatives.ContainsKey("Jewelery")) initiatives.Add("Jewelery", false);
        if (initiatives["Jewelery"] == false)
        {
            curInitiative = "Jewelery";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Ювелирное дело";
        describe.text = "Обучить людей огранке драгоценных камней.\n" + "Каждый месяц в казну дополнительно поступает 5 золотых монет, повышает коррупцию.\n\n" + "Цена инициативы: " + price;
    }

    public void Instruments()
    {
        typeInitiative = "Ore";
        if (!initiatives.ContainsKey("Instruments")) initiatives.Add("Instruments", false);
        if (initiatives["Instruments"] == false)
        {
            curInitiative = "Instruments";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Инструменты";
        describe.text = "Изготовить новые рудодобывающие инструменты.\n" + "Повышает поддержку населения рудодобывающих поселений.\n\n" + "Цена инициативы: " + price;
    }

    public void GovernmentOrder()
    {
        typeInitiative = "Ore";
        if (!initiatives.ContainsKey("GovernmentOrder")) initiatives.Add("GovernmentOrder", false);
        if (initiatives["GovernmentOrder"] == false)
        {
            curInitiative = "GovernmentOrder";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Государственный заказ";
        describe.text = "Подписать договор о поставке металла в столицу для изготовления оружия.\n" + "Повышает агрессию мятежников, повышает эффективность государственных войск.\n\n" + "Цена инициативы: " + price;
    }

    public void MaritimeCouncil()
    {
        typeInitiative = "Fish";
        if (!initiatives.ContainsKey("MaritimeCouncil")) initiatives.Add("MaritimeCouncil", false);
        if (initiatives["MaritimeCouncil"] == false)
        {
            curInitiative = "MaritimeCouncil";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Морской совет";
        describe.text = "Собрать совет глав морских поселений, чтобы определить их нужды.\n" + "Повышает поддержку населения рыболовных поселений.\n\n" + "Цена инициативы: " + price;
    }

    public void FishingEquipment1()
    {
        typeInitiative = "Fish";
        if (!initiatives.ContainsKey("FishingEquipment1")) initiatives.Add("FishingEquipment1", false);
        if (initiatives["FishingEquipment1"] == false)
        {
            curInitiative = "FishingEquipment1";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рыболовный инвентарь 1";
        describe.text = "Отремонтировать старые рыболовные снасти, чтобы улучшить эффективность рыбной ловни.\n" + "Повышает поддержку населения рыболовных поселений.\n\n" + "Цена инициативы: " + price;
    }

    public void FishingEquipment2()
    {
        typeInitiative = "Fish";
        if (!initiatives.ContainsKey("FishingEquipment2")) initiatives.Add("FishingEquipment2", false);
        if (initiatives["FishingEquipment2"] == false)
        {
            curInitiative = "FishingEquipment2";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рыболовный инвентарь 2";
        describe.text = "Изготовить рыболовные снасти по современным стандартам.\n" + "Повышает поддержку населения рыболовных поселений, повышает коррупцию.\n\n" + "Цена инициативы: " + price;
    }

    public void IllegalFishing()
    {
        typeInitiative = "Fish";
        if (!initiatives.ContainsKey("IllegalFishing")) initiatives.Add("IllegalFishing", false);
        if (initiatives["IllegalFishing"] == false)
        {
            curInitiative = "IllegalFishing";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Борьба с незаконной ловлей";
        describe.text = "Предотвратить незаконный вылов дорогой рыбы.\n" + "Повышает поддержку населения рыболовных поселений, повышает агрессию мятежников.\n\n" + "Цена инициативы: " + price;
    }

    public void ExportTrade()
    {
        typeInitiative = "Agriculture";
        if (!initiatives.ContainsKey("ExportTrade")) initiatives.Add("ExportTrade", false);
        if (initiatives["ExportTrade"] == false)
        {
            curInitiative = "ExportTrade";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Экспорт и торговля";
        describe.text = "Организовать торговые караваны в отдаленные регионы государства.\n" + "Незначительно повышает поддержку всех регионов, повышает коррупцию.\n\n" + "Цена инициативы: " + price;
    }

    public void Shipbuilding()
    {
        typeInitiative = "Fish";
        if (!initiatives.ContainsKey("Shipbuilding")) initiatives.Add("Shipbuilding", false);
        if (initiatives["Shipbuilding"] == false)
        {
            curInitiative = "Shipbuilding";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Судостроение";
        describe.text = "Построить верфь для своевременного ремонта суден и постройки новых.\n" + "Значительно повышает поддержку рыболовных поселений, значительно повышает коррупцию.\n\n" + "Цена инициативы: " + price;
    }

    public void Pastures()
    {
        typeInitiative = "Agriculture";
        if (!initiatives.ContainsKey("Pastures")) initiatives.Add("Pastures", false);
        if (initiatives["Pastures"] == false)
        {
            curInitiative = "Pastures";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Пастбища";
        describe.text = "Отстроить пастбища для развития скотоводства.\n" + "Увеличивает поддержку населения сельскозозяйственных поселений.\n\n" + "Цена инициативы: " + price;
    }

    public void Markets()
    {
        typeInitiative = "Agriculture";
        if (!initiatives.ContainsKey("Markets")) initiatives.Add("Markets", false);
        if (initiatives["Markets"] == false)
        {
            curInitiative = "Markets";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рынки";
        describe.text = "Открыть рынки с мясными и хлебными изделиями.\n" + "Увеличивает поддержку населения сельскозозяйственных поселений, повышает коррупцию.\n\n" + "Цена инициативы: " + price;
    }
    #endregion

    #region Правительственные инициативы

    public void Сorruption1()
    {
        nameText.text = "Борьба с коррупцией 1";
        describe.text = "Организовать принятие жалоб на коррупцию от местных людей со стороны старших чинов в деревнях.\n" + "Постепенно снижает коррупцию.";
    }

    public void Сorruption2()
    {
        nameText.text = "Борьба с коррупцией 2";
        describe.text = "Устроить показательные суды и разжалования старших чинов, чтобы глашатаи и граждане могли видеть как правитель борется с коррумпированными лицами.\n" + "Постепенно снижает коррупцию.";
    }

    public void Сorruption3()
    {
        nameText.text = "Борьба с коррупцией 3";
        describe.text = "Нанять верных людей для слежки, которые будут доносить напрямую о коррумпированных людях.\n" + "Постепенно снижает коррупцию.";
    }

    public void Сorruption4()
    {
        nameText.text = "Борьба с коррупцией 4";
        describe.text = "Запретить валюту крупного достоинства в рамках широкой демонетизации. Постепенно снижает коррупцию.\n" + "Постепенно снижает коррупцию.";
    }


    public void ShoppingChannels()
    {
        nameText.text = "Эффективные торговые каналы";
        describe.text = "Назначить надежных людей, чтобы очистить каналы закупок и усложнить процесс получения \"откатов\" от сделок и договоров.\n" + "Приводит к меньшему росту коррупции.";
    }
    
    public void EliminationСorruption1()
    {
        nameText.text = "Искоренение коррупции 1";
        describe.text = "Приказать открыто объявлять имена лиц, подозреваемых в коррупционных правонарушениях.\n" + "Один раз снижает риск коррупции и на время значительно снижает коррупцию.";
    }

    public void EliminationСorruption2()
    {
        nameText.text = "Искоренение коррупции 2";
        describe.text = "Приказать открыто объявлять имена лиц, подозреваемых в коррупционных правонарушениях.\n" + "Один раз снижает риск коррупции и на время значительно снижает коррупцию.";
    }

    public void DiplomaticRepresentatives()
    {
        nameText.text = "Районные дипломатические представители";
        describe.text = "Назначить местных дипломатических представителей в различные зоны, чтобы они общались с местным обществом и собирали информацию о происходящем.\n" + "---------";
    }

    public void SocialAnalytics()
    {
        nameText.text = "Социальная аналитика";
        describe.text = "Назначить к дипломатическим представителям социальных аналитиков для подробных сборов данных о населении зон.\n" + "Ускоряет постепенный сбор разведданных о зонах.";
    }

    public void Heralds()
    {
        nameText.text = "Связь с общественностью и глашатаи";
        describe.text = "Создать отдел по связям с общественностью и местными глашатаями для оповещения населения об успехах, управления общественным мнением и пресечения нападок. \n" + "Значительно преумножает уровень поддержки в средних и поздних этапах.";
    }

    public void StrategicCommunication()
    {
        nameText.text = " Стратегическая коммуникация";
        describe.text = "Использовать развешивание объявлений, доску новостей в деревнях для создания положительного мнения у жителей.\n" + "Преумножает уровень поддержки на среднем и позднем этапе.";
    }

    public void LocalMilitia()
    {
        nameText.text = "Местное ополчение";
        describe.text = "Нанять и вооружить местных крестьян для поддержания мира.\n" + "Улучшает безопасность в зоне, благодаря чему мятежникам сложнее её захватить, но незначительно снижает уровень поддержки.";
    }

    public void LocalSecurity()
    {
        nameText.text = "Набор местной охраны ";
        describe.text = "Нанять новых охранников правопорядка и обучить их современным методам и стандартам обеспечения безопасности.\n" + "Улучшает безопасность в зоне, благодаря чему мятежникам становится сложнее её захватить. Повышает уровень поддержки.";
    }

    public void SecurityУExtension()
    {
        nameText.text = "Расширение местной охраны";
        describe.text = "Нанять советника по охране правопорядка и закупить лучшее снаряжение и оружие.\n" + "Улучшает безопасность в зоне, благодаря чему мятежникам её сложнее захватить. Повышает уровень поддержки.";
    }

    public void CommunityWork()
    {
        nameText.text = "Работа с местными сообществами";
        describe.text = "Сформировать группу советников, которая займется поиском оптимальных путей для внедрения инициатив в регионы.\n" + "Слегка ускоряет внедрение инициатив в отдельных типах зон.";
    }

    public void FishingSettlements1()
    {
        nameText.text = "Работа с рыболовными поселениями 1";
        describe.text = "Направить группы советников в рыболовные зоны поселений, чтобы жители могли получить все преимущества гражданских инициатив.\n" + "Ускоряет внедрение инициатив в рыбловных зонах.";
    }

    public void FishingSettlements2()
    {
        nameText.text = "Работа с рыболовными поселениями 2";
        describe.text = "Расширить возможности группы советников по работе с рыболовными зонами.\n" + "Ускоряет внедрение инициатив в рыболовных зонах.";
    }

    public void MiningZones1()
    {
        nameText.text = "Работа с горнодобывающими зонами 1";
        describe.text = "Направить группы советников в горнодобывающие зоны, чтобы жители могли получить все преимущества гражданских инициатив.\n" + "Ускоряет внедрение инициатив в горнодобывающих зонах.";
    }

    public void MiningZones2()
    {
        nameText.text = "Работа с горнодобывающими зонами 2";
        describe.text = "Расширить возможности группы советников по работе с горнодобывающих зонами.\n" + "Ускоряет внедрение инициатив в горнодобывающих зонах.";
    }

    public void AgriculturalAreas1()
    {
        nameText.text = "Работа с сельскохозяйственными зонами 1";
        describe.text = "Направить группы советников в сельскохозяйственные зоны, чтобы жители могли получить все преимущества гражданских инициатив.\n" + "Ускоряет внедрение инициатив в сельскохозяйственных зонах.";
    }

    public void AgriculturalAreas2()
    {
        nameText.text = "Работа с сельскохозяйственными зонами 1";
        describe.text = "Расширить возможности группы советников по работе с сельскохозяйственными зонами.\n" + "Ускоряет внедрение инициатив в сельскохозяйственных зонах.";
    }

    public void LinksOrders()
    {
        nameText.text = "Связи между орденами";
        describe.text = "Наладить дипломатические связи. Вступить в диалог с представителями враждебных орденов, чтобы ограничить поддержку мятежников.\n" + "Открывает дополнительные дипломатические варианты. Эффект растет со временем.";
    }

    public void Equality()
    {
        nameText.text = "Равенство перед законом";
        describe.text = "Открыто объявить, что перед правосудием все будут равны. Все должны нести ответственность за свои поступки, независимо от должности и статуса.\n" + "Значительно повышает уровень поддержки и дает 3 ед. репутации.";
    }

    public void Diplomacy()
    {
        nameText.text = "Переход к дипломатии";
        describe.text = "Открыто выступить за демократические выборы. Установив успешную демократию, вы со временем значительно увеличите свою репутацию.\n" + "Дает 2 ед. репутации.";
    }

    public void OrderHelp()
    {
        nameText.text = "Помощь от серебряного ордена";
        describe.text = "Обратиться за помощью к серебряному ордену для помощи в стабилизации регионов.\n" + "Предоставляет дополнительную помощь в деле стабилизации региона.";
    }
    #endregion

    #region Военные инициативы

    public void SilverKnights1()
    {
        if(!initiatives.ContainsKey("SilverKnights1")) initiatives.Add("SilverKnights1", false);
        if (initiatives["SilverKnights1"] == false)
        {
            curInitiative = "SilverKnights1";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Серебрянные рыцари 1";
        describe.text = "Запросить элитное подразделение рыцарей с ограниченным периодом службы.\n" + "Быстрое и эффективное в бою подразделение, но может вызывать недовольство населения.\n\n" + "Цена инициативы: " + price;
    }

    public void SilverKnights2()
    {
        if (!initiatives.ContainsKey("SilverKnights2")) initiatives.Add("SilverKnights2", false);
        if (initiatives["SilverKnights2"] == false)
        {
            curInitiative = "SilverKnights2";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Серебрянные рыцари 2";
        describe.text = "Запросить элитное подразделение рыцарей с ограниченным периодом службы.\n" + "Быстрое и эффективное в бою подразделение, но может вызывать недовольство населения.\n\n" + "Цена инициативы: " + price;
    }

    public void SilverKnights3()
    {
        if (!initiatives.ContainsKey("SilverKnights3")) initiatives.Add("SilverKnights3", false);
        if (initiatives["SilverKnights3"] == false)
        {
            curInitiative = "SilverKnights3";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Серебрянные рыцари 3";
        describe.text = "Запросить элитное подразделение рыцарей с ограниченным периодом службы.\n" + "Быстрое и эффективное в бою подразделение, но может вызывать недовольство населения.\n\n" + "Цена инициативы: " + price;
    }

    public void SilverKnights4()
    {
        if (!initiatives.ContainsKey("SilverKnights4")) initiatives.Add("SilverKnights4", false);
        if (initiatives["SilverKnights4"] == false)
        {
            curInitiative = "SilverKnights4";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Серебрянные рыцари 4";
        describe.text = "Запросить элитное подразделение рыцарей с ограниченным периодом службы.\n" + "Быстрое и эффективное в бою подразделение, но может вызывать недовольство населения.\n\n" + "Цена инициативы: " + price;
    }

    public void SilverKnights5()
    {
        if (!initiatives.ContainsKey("SilverKnights5")) initiatives.Add("SilverKnights5", false);
        if (initiatives["SilverKnights5"] == false)
        {
            curInitiative = "SilverKnights5";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Серебрянные рыцари 5";
        describe.text = "Запросить элитное подразделение рыцарей с ограниченным периодом службы.\n" + "Быстрое и эффективное в бою подразделение, но может вызывать недовольство населения.\n\n" + "Цена инициативы: " + price;
    }

    public void KingdomKnights1()
    {
        if (!initiatives.ContainsKey("KingdomKnights1")) initiatives.Add("KingdomKnights1", false);
        if (initiatives["KingdomKnights1"] == false)
        {
            curInitiative = "KingdomKnights1";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рыцари королевства 1";
        describe.text = "Обучить постоянное подразделение рыцарей королевства." + "Обучение займет длительное время. Медленнее и слабее Серебряных рыцарей, но реже вызывают недовольство.\n\n" + "Цена инициативы: " + price;
    }

    public void KingdomKnights2()
    {
        if (!initiatives.ContainsKey("KingdomKnights2")) initiatives.Add("KingdomKnights2", false);
        if (initiatives["KingdomKnights2"] == false)
        {
            curInitiative = "KingdomKnights2";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рыцари королевства 1";
        describe.text = "Обучить постоянное подразделение рыцарей королевства." + "Обучение займет длительное время. Медленнее и слабее Серебряных рыцарей, но реже вызывают недовольство.\n\n" + "Цена инициативы: " + price;
    }

    public void KingdomKnights3()
    {
        if (!initiatives.ContainsKey("KingdomKnights3")) initiatives.Add("KingdomKnights3", false);
        if (initiatives["KingdomKnights3"] == false)
        {
            curInitiative = "KingdomKnights3";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рыцари королевства 1";
        describe.text = "Обучить постоянное подразделение рыцарей королевства." + "Обучение займет длительное время. Медленнее и слабее Серебряных рыцарей, но реже вызывают недовольство.\n\n" + "Цена инициативы: " + price;
    }

    public void KingdomKnights4()
    {
        if (!initiatives.ContainsKey("KingdomKnights4")) initiatives.Add("KingdomKnights4", false);
        if (initiatives["KingdomKnights4"] == false)
        {
            curInitiative = "KingdomKnights4";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рыцари королевства 1";
        describe.text = "Обучить постоянное подразделение рыцарей королевства." + "Обучение займет длительное время. Медленнее и слабее Серебряных рыцарей, но реже вызывают недовольство.\n\n" + "Цена инициативы: " + price;
    }

    public void KingdomKnights5()
    {
        if (!initiatives.ContainsKey("KingdomKnights5")) initiatives.Add("KingdomKnights5", false);
        if (initiatives["KingdomKnights5"] == false)
        {
            curInitiative = "KingdomKnights5";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Рыцари королевства 1";
        describe.text = "Обучить постоянное подразделение рыцарей королевства." + "Обучение займет длительное время. Медленнее и слабее Серебряных рыцарей, но реже вызывают недовольство.\n\n" + "Цена инициативы: " + price;
    }

    public void Bastions()
    {
        if (!initiatives.ContainsKey("Bastions")) initiatives.Add("Bastions", false);
        if (initiatives["Bastions"] == false)
        {
            curInitiative = "Bastions";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Бастионы";
        describe.text = "Разрешить строительство бастионов в разведанных зонах. Подходящие области будут определены со временем.\n" + "Бастионы предоставляют разведданные и военную поддержку ближайшим отрядам рыцарей.\n\n" + "Цена инициативы: " + price;
    }

    public void DetachmentExpansion()
    {
        if (!initiatives.ContainsKey("DetachmentExpansion")) initiatives.Add("DetachmentExpansion", false);
        if (initiatives["DetachmentExpansion"] == false)
        {
            curInitiative = "DetachmentExpansion";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Расширение воинских отрядов";
        describe.text = "Внедрить в отряды рыцарей королевства метких лучников, помогающих в наступлении и прикрывающих рыцарей с расстояния.\n" + "Значительно усиливает рыцарей королевства.\n\n" + "Цена инициативы: " + price;
    }

    public void Logistics()
    {
        if (!initiatives.ContainsKey("Logistics")) initiatives.Add("Logistics", false);
        if (initiatives["Logistics"] == false)
        {
            curInitiative = "Logistics";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Бастионы";
        describe.text = "Снабдить рыцарей королевства верховым транспортом для повышения их мобильности в труднопроходимой местности.\n\n" + "Цена инициативы: " + price;
    }

    public void RangedProtection()
    {
        if (!initiatives.ContainsKey("RangedProtection")) initiatives.Add("RangedProtection", false);
        if (initiatives["RangedProtection"] == false)
        {
            curInitiative = "RangedProtection";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Защита от оружий дальнего боя";
        describe.text = "Обеспечить рыцарей королевства тяжелой бронёй и щитами, чтобы защитить их от засад и стремительных атак вражеских лучников.\n" + "Значительно усиливает рыцарей королевства.\n\n" + "Цена инициативы: " + price;
    }

    public void Reinforcements()
    {
        if (!initiatives.ContainsKey("Reinforcements")) initiatives.Add("Reinforcements", false);
        if (initiatives["Reinforcements"] == false)
        {
            curInitiative = "Reinforcements";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Подкрепления";
        describe.text = "Приказать бастионам поддерживать связь с рыцарями для своевременного оказания им военной помощи.\n" + "Значительно увеличивает боевые бонусы, которые рыцари получают от гарнизонов.\n\n" + "Цена инициативы: " + price;
    }

    public void PeopleSupport()
    {
        if (!initiatives.ContainsKey("PeopleSupport")) initiatives.Add("PeopleSupport", false);
        if (initiatives["PeopleSupport"] == false)
        {
            curInitiative = "PeopleSupport";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Поддержка населения";
        describe.text = "Рыцари бастионов сотрудничают с местными сообществами и помогают в обеспечении безопасности, охраняя общественные места, такие как рынки и правительственные здания.\n" + "Повышает местный уровень поддержки в зонах с бастионами.\n\n" + "Цена инициативы: " + price;
    }

    public void CrossingPoints()
    {
        if (!initiatives.ContainsKey("CrossingPoints")) initiatives.Add("CrossingPoints", false);
        if (initiatives["CrossingPoints"] == false)
        {
            curInitiative = "CrossingPoints";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Контрольно-пропускные пункты";
        describe.text = "Гарнизоны устраивают КПП по всей зоне для обнаружения и пресечения вражеской деятельности.\n" + "Улучшает безопасность в зоне.\n\n" + "Цена инициативы: " + price;
    }

    public void Diplomats()
    {
        if (!initiatives.ContainsKey("Diplomats")) initiatives.Add("Diplomats", false);
        if (initiatives["Diplomats"] == false)
        {
            curInitiative = "Diplomats";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Дипломаты и проводники";
        describe.text = "Назначить в подразделения дипломатов и проводников, чтобы облегчить взаимодействие с местными старостами деревень.\n" + "Рыцари быстрее собирают разведданные и повышают местный уровень поддержки.\n\n" + "Цена инициативы: " + price;
    }

    public void LocalKnowledge()
    {
        if (!initiatives.ContainsKey("LocalKnowledge")) initiatives.Add("LocalKnowledge", false);
        if (initiatives["LocalKnowledge"] == false)
        {
            curInitiative = "LocalKnowledge";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Знание местности";
        describe.text = "Назначить в каждый рыцарский отряд подразделение опытных следопытов. Рыцари быстрее собирают разведданные.\n" + "Серебряные рыцари значительно реже вызывают недовольство населения.\n\n" + "Цена инициативы: " + price;
    }

    public void HelpToPeople()
    {
        if (!initiatives.ContainsKey("HelpToPeople")) initiatives.Add("HelpToPeople", false);
        if (initiatives["HelpToPeople"] == false)
        {
            curInitiative = "HelpToPeople";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Помощь местному населению";
        describe.text = "Разрешить отрядам рыцарей помогать местным сообществам в промежутках между сражениями и сбором разведданных.\n" + "Рыцари помогают внедрять инициативы и повышают местный уровень поддержки.\n\n" + "Цена инициативы: " + price;
    }

    public void EnhancedHelp()
    {
        if (!initiatives.ContainsKey("EnhancedHelp")) initiatives.Add("EnhancedHelp", false);
        if (initiatives["EnhancedHelp"] == false)
        {
            curInitiative = "EnhancedHelp";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Усиленная помощь";
        describe.text = "Поручить рыцарям помогать местным работникам с оборудованием, благоустройством и поставкой ресурсов в критически важных областях.\n" + "Рыцари помогают внедрять инициативы и повышают местный уровень поддержки.\n\n" + "Цена инициативы: " + price;
    }

    public void Scouts()
    {
        if (!initiatives.ContainsKey("Scouts")) initiatives.Add("Scouts", false);
        if (initiatives["Scouts"] == false)
        {
            curInitiative = "Scouts";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Отряды разведчиков";
        describe.text = "Разрешить использование профессиональных разведчиков для получения разведданных и выслеживания угроз\n" + "Дает временные разведданные о зоне и определяет вражеские действия.\n\n" + "Цена инициативы: " + price;
    }

    public void SharpenedStrategy()
    {
        if (!initiatives.ContainsKey("SharpenedStrategy")) initiatives.Add("SharpenedStrategy", false);
        if (initiatives["SharpenedStrategy"] == false)
        {
            curInitiative = "SharpenedStrategy";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Заточенная стратегия";
        describe.text = "Ускоряют перезарядку отрядов разведчиков между вылазками.\n\n" + "Цена инициативы: " + price;
    }

    public void PreparationOuting()
    {
        if (!initiatives.ContainsKey("PreparationOuting")) initiatives.Add("PreparationOuting", false);
        if (initiatives["PreparationOuting"] == false)
        {
            curInitiative = "PreparationOuting";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Подготовка перед вылазкой";
        describe.text = "Увеличивает время пребывания отряда разведчиков в зоне.\n\n" + "Цена инициативы: " + price;
    }

    public void Sabotage()
    {
        if (!initiatives.ContainsKey("Sabotage")) initiatives.Add("Sabotage", false);
        if (initiatives["Sabotage"] == false)
        {
            curInitiative = "Sabotage";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Диверсия";
        describe.text = "Отряды разведчиков ослабляют противника.\n\n" + "Цена инициативы: " + price;
    }

    public void VolleyСatapult()
    {
        if (!initiatives.ContainsKey("VolleyСatapult")) initiatives.Add("VolleyСatapult", false);
        if (initiatives["VolleyСatapult"] == false)
        {
            curInitiative = "VolleyСatapult";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Залп катапульт";
        describe.text = "Разрешить проводить обстрелы катапультами сил мятежников.\n" + "Дает значительные боевые бонусы к силе рыцарей. Внимание! Может привести к жертвам среди мирного населения.\n\n" + "Цена инициативы: " + price;
    }

    public void AccurateAiming1()
    {
        if (!initiatives.ContainsKey("AccurateAiming1")) initiatives.Add("AccurateAiming1", false);
        if (initiatives["AccurateAiming1"] == false)
        {
            curInitiative = "AccurateAiming1";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Точная наводка 1";
        describe.text = "Назначить наводчиков на отряды катапульт./n" + "Снижает риск жертв среди мирного населения.\n\n" + "Цена инициативы: " + price;
    }

    public void AccurateAiming2()
    {
        if (!initiatives.ContainsKey("AccurateAiming2")) initiatives.Add("AccurateAiming2", false);
        if (initiatives["AccurateAiming2"] == false)
        {
            curInitiative = "AccurateAiming2";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Точная наводка 2";
        describe.text = "Назначить наводчиков на отряды катапульт./n" + "Снижает риск жертв среди мирного населения.\n\n" + "Цена инициативы: " + price;
    }

    public void ImprovedMechanism1()
    {
        if (!initiatives.ContainsKey("ImprovedMechanism1")) initiatives.Add("ImprovedMechanism1", false);
        if (initiatives["ImprovedMechanism1"] == false)
        {
            curInitiative = "ImprovedMechanism1";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Усовершенствованный механизм 1";
        describe.text = "Ускоряет перезарядку катапульт.\n\n" + "Цена инициативы: " + price;
    }

    public void ImprovedMechanism2()
    {
        if (!initiatives.ContainsKey("ImprovedMechanism2")) initiatives.Add("ImprovedMechanism2", false);
        if (initiatives["ImprovedMechanism2"] == false)
        {
            curInitiative = "ImprovedMechanism2";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Усовершенствованный механизм 2";
        describe.text = "Ускоряет перезарядку катапульт.\n\n" + "Цена инициативы: " + price;
    }

    public void ExplosiveNuclei()
    {
        if (!initiatives.ContainsKey("ExplosiveNuclei")) initiatives.Add("ExplosiveNuclei", false);
        if (initiatives["ExplosiveNuclei"] == false)
        {
            curInitiative = "ExplosiveNuclei";
            button.interactable = true;
            buttonText.text = "Приобрести";
            price = playerInfo.priceInitiatives + 2 * playerInfo.numberOfInflation;
            if (playerInfo.coins < price)
            {
                button.interactable = false;
                buttonText.text = "Нужно больше золота";
            }
        }
        else
        {
            button.interactable = false;
            buttonText.text = "Уже имеется";
        }
        nameText.text = "Взрывные ядра";
        describe.text = "Значительно усиливает обстрел катапультами, но увеличивает риск жертв среди мирного населения.\n\n" + "Цена инициативы: " + price;
    }
    #endregion

    public void BuyInitiative()
    {
        if (curInitiative != null)
        {
            initiatives[curInitiative] = true;
            button.interactable = false;
            CreatingUI();
            playerInfo.numberOfInflation++;
            playerInfo.coins -= price;
            UICtrl.UpdateCoins();
            SupportUp();
            coins.text = "" + playerInfo.coins;
            buttonText.text = "Уже имеется";
            if (transform.Find("Border/Initiatives/" + curInitiative).GetComponent<Addiction>())
                transform.Find("Border/Initiatives/" + curInitiative).GetComponent<Addiction>().UnlockInitia();
            curInitiative = null;
            typeInitiative = "";
            UpdateSpecifications();
        }
    }

    private void CreatingUI()
    {
        if(curInitiative.Contains("SilverKnights"))
        {
            UICtrl.CreatingArmy(curInitiative);
        }
        if(curInitiative.Contains("KingdomKnights"))
        {
            UICtrl.CreatingArmy(curInitiative);
        }
    }

    public void SaveData()
    {
        var toJson = "";
        data.keys.Clear();
        data.values.Clear();
        foreach (KeyValuePair<string, bool> init in initiatives)
        {
            data.keys.Add(init.Key);
            data.values.Add(init.Value);
        }
        toJson = JsonUtility.ToJson(data);
        using (StreamWriter file = new StreamWriter(Application.dataPath + "/StreamingAssets/BoughtInitiatives.json"))
            file.Write(toJson);
    }

    public void LoadData()
    {
        if(File.Exists(Application.dataPath + "/StreamingAssets/BoughtInitiatives.json"))
        {
            string fromJson = "";
            using (StreamReader sr = new StreamReader(Application.dataPath + "/StreamingAssets/BoughtInitiatives.json"))
            {
                fromJson = sr.ReadToEnd();
            }
            data = JsonUtility.FromJson<DataInitiatives>(fromJson);
            initiatives = data.keys.Zip(data.values, (k, v) => new { Key = k, Value = v }).ToDictionary(initiatives => initiatives.Key, initiatives => initiatives.Value);
        } 
    }

    private void SupportUp()
    {
        if(typeInitiative == "Ore")
        {
            playerInfo.numberInitiativesO++;
            playerInfo.SupportO += 100 * playerInfo.numberInitiativesO;
        }
        if (typeInitiative == "Fish")
        {
            playerInfo.numberInitiativesF++;
            playerInfo.SupportF += 100 * playerInfo.numberInitiativesF;
        }
        if (typeInitiative == "Agriculture")
        {
            playerInfo.numberInitiativesA++;
            playerInfo.SupportA += 100 * playerInfo.numberInitiativesA;
        }
    }

    public void UpdateButtons()
    {
        foreach (Addiction each in listInitiatives)
        {
            each.UnlockInitia();
        }
    }

    private void UpdateSpecifications()
    {
        supportFill.fillAmount = (playerInfo.SupportA + playerInfo.SupportF + playerInfo.SupportO) / playerInfo.maxSup;
        inflationFill.fillAmount = playerInfo.numberOfInflation / playerInfo.maxinflation;
    }

    public void CloseWindow()
    {
        GetComponent<UIButtons>().Continue();
        SaveData();
    }
}

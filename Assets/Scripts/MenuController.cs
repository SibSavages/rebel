﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private GameObject SceneLoader;
    [SerializeField] public GameObject loadingInfo, loadingIcon;

    private void Start()
    {
        Time.timeScale = 1;
    }

    public void NewGame()
    {
        if (File.Exists(Application.dataPath + "/StreamingAssets/BoughtInitiatives.json"))
            File.Delete(Application.dataPath + "/StreamingAssets/BoughtInitiatives.json");
        Instantiate(SceneLoader, canvas.transform.position, Quaternion.identity, canvas.transform);
        Instantiate(loadingInfo, canvas.transform.position + new Vector3(0, -Screen.height / 2.5f, 0), Quaternion.identity, canvas.transform);
        Instantiate(loadingIcon, canvas.transform.position + new Vector3(Screen.width/2.3f, Screen.height/2.5f, 0), Quaternion.identity, canvas.transform);
    }

    public void Settings()
    {

    }

    public void Shop()
    {

    }
}

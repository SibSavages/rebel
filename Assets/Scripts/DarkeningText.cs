﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DarkeningText : MonoBehaviour
{
    [SerializeField] private bool visible;
    [SerializeField] private float scaleDark;
    private Text text;
    private Color color;
    private bool access = true;

    void Start()
    {
        text = GetComponent<Text>();
        color = text.color;
    }

    void Update()
    {
        if (access)
        {
            if (visible)
            {
                color.a -= Time.deltaTime * 0.5f * scaleDark;
                if (color.a <= 0)
                {
                    visible = false;
                    access = false;
                }
            }
            else
            {
                color.a += Time.deltaTime * 0.5f * scaleDark;
                if (color.a >= 1)
                {
                    visible = true;
                    access = false;
                }
            }
            text.color = color;
        }
    }
}

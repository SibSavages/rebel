﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Addiction : MonoBehaviour
{
    [SerializeField] public GameObject[] nextInitia;
    [SerializeField] public Initiatives data;
    public string nameInitia;

    public void UnlockInitia()
    {
        if (data.initiatives.ContainsKey(nameInitia))
        {
            if (data.initiatives[nameInitia])
            {
                foreach(GameObject initia in nextInitia)
                initia.SetActive(true);
            }
        }
    }
}

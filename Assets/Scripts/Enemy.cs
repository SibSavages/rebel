﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public GameObject curVillage;
    public float stoppingDistance;
    private NavMeshAgent navAgent;
    public List<Transform> stayTargets;
    public string state = "Idle";
    public Transform target;
    private float stayTimeKD;
    private float stayTime;


    void Start()
    {
        stayTimeKD = 7 * Random.Range(4, 6);
        stayTime = stayTimeKD;
        navAgent = GetComponent<NavMeshAgent>();
        target = stayTargets[Random.Range(0, stayTargets.Count)];
    }

    void Update()
    {
        StateTimer();
        Movement();
        CaptureZone();
    }

    private void Movement()
    {
        if (state == "Idle")
        {
            navAgent.SetDestination(target.position);
            ChangeTarget();
        }
        if(state =="GoCapture")
        {
            navAgent.SetDestination(target.position);
            if(Vector3.Distance(transform.position, target.position) <= stoppingDistance)
            {
                CaptureZone();
            }
            ChangeTarget();
        }
    }

    private void ChangeTarget()
    {
        if(Vector3.Distance(transform.position, target.position) <= stoppingDistance)
        {
            target = stayTargets[Random.Range(0, stayTargets.Count)];
        }
    }

    private void StateTimer()
    {
        if(state =="Idle")
        {
            stayTime -= Time.deltaTime;
            if (stayTime<=0)
            {
                ChangeVillage();
            }
        }
    }

    private void ChangeVillage()
    {
        List<GameObject> nVillages = curVillage.GetComponent<Village>().neighborVillages;
        while (state == "Idle")
        {
            int nextVillage = Random.Range(0, nVillages.Count);
            if (nVillages[nextVillage].GetComponent<Village>().state == "neutral")
            {
                curVillage = nVillages[nextVillage];
                stayTargets = curVillage.GetComponent<Village>().targets;
                target = stayTargets[Random.Range(0, stayTargets.Count)];
                state = "GoCapture";
                stayTime = stayTimeKD;
            }
        }
    }

    private void CaptureZone()
    {
        if(curVillage.GetComponent<Village>().state == "neutral")
            curVillage.GetComponent<Village>().Capture();
        else state = "Idle";
    }
}

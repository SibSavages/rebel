﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private string scene = "Game";
    private AsyncOperation async;
    private Image image;
    private Color color;

    private void Start()
    {
        image = gameObject.GetComponent<Image>();
        async = SceneManager.LoadSceneAsync(scene);
        async.allowSceneActivation = false;
    }

    void Update()
    {
        color = image.color;
        if (async.progress >= 0.9f && color.a>=1)
        {
            async.allowSceneActivation = true;
        }
    }
}

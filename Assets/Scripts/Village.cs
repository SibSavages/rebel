﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Village : MonoBehaviour
{
    [SerializeField] private PlayerInfo playerInfo;
    [SerializeField] private UIController UICtrl;
    public List<Transform> targets;
    public GameObject zoneButton;
    public string state = "neutral";
    public string nameVillage;
    public string typeVillage;
    public List<GameObject> neighborVillages;
    public float population;
    public float regionStability;
    public float stablePopulation;

    private void Start()
    {
        nameVillage = gameObject.name;
    }

    public void Capture()
    {
        state = UICtrl.CaptureZone(gameObject);
    }

    public void Stabilization()
    {
        if(typeVillage == "Fish")
        {
            stablePopulation += playerInfo.SupportF;
        }
        if (typeVillage == "Ore")
        {
            stablePopulation += playerInfo.SupportO;
        }
        if (typeVillage == "Agriculture")
        {
            stablePopulation += playerInfo.SupportA;
        }
    }
}

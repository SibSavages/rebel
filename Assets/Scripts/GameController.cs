﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] private EnemyController enemyCtrl;
    [SerializeField] private UIController UICtrl;
    [SerializeField] private EventController eventCtrl;
    [SerializeField] private PlayerInfo player;
    [SerializeField] private Image fillMonth;
    private float monthTime = 0f;
    private float inflationKD;

    private void Start()
    {
        inflationKD = player.monthKD;
    }

    void Update()
    {
        UpdateGameCtrl();
        eventCtrl.UpdateEvents();
        enemyCtrl.EnemyCtrlUpdate();
        UICtrl.UpdateUICtrl();
    }

    private void UpdateGameCtrl()
    {
        TimeGo();
        UpdateInflation();
    }

    private void TimeGo()
    {
        monthTime += Time.deltaTime;
        if (monthTime>= player.monthKD)
        {
            monthTime = 0;
            if(player.numbMonth>=11)
            {
                player.numbMonth = 0;
                player.year++;
            }
            else
                player.numbMonth++;
            player.curMonth = player.months[player.numbMonth];
            player.coins += player.income;
            UICtrl.UpdateCoins();
            UICtrl.UpdateDate();
            UpdateStability();
        }
        if (fillMonth.fillAmount >= 1)
            fillMonth.fillAmount = 0;
        else
            fillMonth.fillAmount += Time.deltaTime / player.monthKD;
    }

    public void UpdateInflation()
    {
        if(player.numberOfInflation > 0)
        {
            inflationKD -= Time.deltaTime;
            if(inflationKD<=0)
            {
                player.numberOfInflation--;
                inflationKD = player.monthKD;
            }
        }
    }

    public void UpdateStability()
    {
        float stablePeople = 0;
        for(int i = 0; i<player.village.Length; i++)
        {
            player.village[i].Stabilization();
            player.village[i].regionStability = (float)(player.village[i].stablePopulation / player.village[i].population);
            stablePeople += player.village[i].stablePopulation;
        }
        player.generalStability = (float)(stablePeople / player.population);
        if(player.generalStability > 100)
        {
            player.generalStability = 100;
        }
        UICtrl.UpdateStable();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private GameObject canvas;
    [SerializeField] private GameObject blackBack;
    [SerializeField] private Image fillStable;
    [SerializeField] private Text textStable;
    [SerializeField] private PlayerInfo player;
    [SerializeField] private GameObject[] zone;
    [SerializeField] private GameObject peaceWind;
    [SerializeField] private GameObject warWind;
    [SerializeField] private GameObject settings;
    [SerializeField] private GameObject tutorial;
    [SerializeField] private Text textCoins;
    [SerializeField] private Text textDate;
    [SerializeField] private Color captColor;
    [SerializeField] private Color backCaptColor;
    [SerializeField] private float captAlpha;
    [SerializeField] private GameObject armyUI;
    [SerializeField] private Image fillReputation;
    [SerializeField] private Text textReputation;
    [SerializeField] private GameObject victoryWindow;
    [SerializeField] private GameObject loseWindow;
    private GameObject curArmyUI;
    private Dictionary<string, float> timeCreatingKD = new Dictionary<string, float>();
    private GameObject curWindow;
    public int flickObjs = 0;
 

    private void Start()
    {        
        blackBack.SetActive(true);
        backCaptColor = captColor;
        UpdateDate();
        UpdateCoins();
        timeCreatingKD.Add("SilverKnights", 14f);
        timeCreatingKD.Add("KingdomKnights", 42f);
    }

    public void UpdateUICtrl()
    {
       CheckClick();
    }

    private void CheckClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (flickObjs == 1)
            {
                for (int i = 0; i < zone.Length; i++)
                {
                    zone[i].GetComponent<FlickZone>().StopFlick();
                }
                flickObjs = 0;
            }
        }
    }

    public void CheckFlick()
    {
        if(flickObjs>1)
        {
            for (int i = 0; i < zone.Length; i++)
            {
                zone[i].GetComponent<FlickZone>().StopFlick();
            }           
        }
        flickObjs = 1;
    }

    public void PeaceReforms()
    {
        CheckCurWind();
        curWindow = Instantiate(peaceWind, canvas.transform.position, Quaternion.identity, canvas.transform);
        curWindow.GetComponent<UIButtons>().curWindow = curWindow;
        curWindow.GetComponent<Initiatives>().coins.text = "" + player.coins;
        Time.timeScale = 0;
    }

    public void WarReforms()
    {
        CheckCurWind();
        curWindow = Instantiate(warWind, canvas.transform.position, Quaternion.identity, canvas.transform);
        curWindow.GetComponent<UIButtons>().curWindow = curWindow;
        curWindow.GetComponent<Initiatives>().coins.text = "" + player.coins;
        Time.timeScale = 0;
    }

    public void Settings()
    {
        CheckCurWind();
        curWindow = Instantiate(settings, canvas.transform.position, Quaternion.identity, canvas.transform);
        curWindow.GetComponent<UIButtons>().curWindow = curWindow;
        Time.timeScale = 0;
    }

    public void CreatingArmy(string armyName)
    {
        if (curArmyUI == null)
        {
            curArmyUI = Instantiate(armyUI, new Vector3(Screen.width / 20, Screen.height / 1.7f, 0), Quaternion.identity, canvas.transform);
            curArmyUI.GetComponent<ArmyButton>().curUnit = armyName;
        }
        if (armyName.Contains("SilverKnights"))
        {
            //curArmyUI.GetComponent<ArmyButton>().armyCreating.Add(armyName, timeCreatingKD["SilverKnights"]);
            curArmyUI.GetComponent<ArmyButton>().valueList.Add(timeCreatingKD["SilverKnights"]);
            curArmyUI.GetComponent<ArmyButton>().keysList.Add(armyName);
            curArmyUI.GetComponent<ArmyButton>().UpdateKDtime();
            curArmyUI.GetComponent<ArmyButton>().armyKD.Add(armyName, timeCreatingKD["SilverKnights"]);
        }
        if (armyName.Contains("KingdomKnights"))
        {
            //curArmyUI.GetComponent<ArmyButton>().armyCreating.Add(armyName, timeCreatingKD["KingdomKnights"]);
            curArmyUI.GetComponent<ArmyButton>().valueList.Add(timeCreatingKD["KingdomKnights"]);
            curArmyUI.GetComponent<ArmyButton>().keysList.Add(armyName);
            curArmyUI.GetComponent<ArmyButton>().UpdateKDtime();
            curArmyUI.GetComponent<ArmyButton>().armyKD.Add(armyName, timeCreatingKD["KingdomKnights"]);
        }
    }

    public void Tutorial()
    {
        CheckCurWind();
        curWindow = Instantiate(tutorial, canvas.transform.position, Quaternion.identity, canvas.transform);
        curWindow.GetComponent<UIButtons>().curWindow = curWindow;
        Time.timeScale = 0;
    }

    private void CheckCurWind()
    {
        if(curWindow)
        {
            Destroy(curWindow);
        }
    }

    public void Continue()
    {
        Time.timeScale = 1;
        Destroy(curWindow);
    }

    public string CaptureZone(GameObject village)
    {
        if(captColor.a < captAlpha)
        {
            GameObject button = village.GetComponent<Village>().zoneButton;
            Color color = captColor;
            color.a += Time.deltaTime * (captAlpha / player.monthKD);
            captColor = color;
            button.GetComponent<Image>().color = captColor;
            button.GetComponent<FlickZone>().color = captColor;
            button.GetComponent<FlickZone>().defaultColor = captColor;
            return "neutral";
        }
        captColor = backCaptColor;
        player.reputation -= 3;
        UpdateReputation();
        return "capture";
    }

    public void UpdateStable()
    {
        fillStable.fillAmount = player.generalStability;
        textStable.text = "Общая стабильность: " + Mathf.Round(player.generalStability*100) + "%";
        if(player.generalStability >= 1)
        {
            player.generalStability = 1;
            Time.timeScale = 0;
            Instantiate(victoryWindow, canvas.transform.position, Quaternion.identity, canvas.transform);
        }
    }

    public void UpdateReputation()
    {
        if (player.reputation <= 0)
        {
            player.reputation = 0;
            Time.timeScale = 0;
            Time.timeScale = 0;
            Instantiate(loseWindow, canvas.transform.position, Quaternion.identity, canvas.transform);
        }
        fillReputation.fillAmount = player.reputation / 100;
        textReputation.text = "" + player.reputation;
    }

    public void UpdateCoins()
    {
        textCoins.text = "" + player.coins;
    }

    public void UpdateDate()
    {
        textDate.text = "" + player.curMonth + ", " + player.year + "г.";
    }
}

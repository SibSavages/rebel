﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmyButton : MonoBehaviour
{
    [SerializeField] private Text numberReadyText;
    [SerializeField] private Sprite kingdomSprite;
    [SerializeField] private Sprite silverSprite;
    [SerializeField] private GameObject button;
    public List<float> valueList = new List<float>();
    public List<string> keysList = new List<string>();
    public string curUnit;
    public Dictionary<string, float> armyCreating = new Dictionary<string, float>();
    public Dictionary<string, float> armyKD = new Dictionary<string, float>();
    public int numberOfReady = 0;
    private float minTime = 100;

    private void Start()
    {
        transform.SetSiblingIndex(6);
    }

    void Update()
    {
        TimerAndFill();
    }

    private void TimerAndFill()
    {
        if(armyCreating.Count > 0)
        {
            numberOfReady = 0;
            //отсчёт времени у каждого юнита
            for(int k = 0; k< valueList.Count; k++)
            {
                if(valueList[k] > 0)
                {
                    valueList[k] -= Time.deltaTime;
                }
            }
            /*
            foreach (string keyArmy in armyCreating.Keys)
            {
                keysList.Add(keyArmy);
            }*/
            armyCreating.Clear();
            //составление словаря
            for (int k = 0; k < valueList.Count; k++)
            {
                armyCreating.Add(keysList[k], valueList[k]);
            }
            foreach (KeyValuePair<string, float> pair in armyCreating)
            {
                if (pair.Value <= 0 && armyCreating.Count >= 2)
                {
                    minTime = 100;
                }
                //проверка на минимумальное время и замена изображения
                if(pair.Value < minTime && pair.Value > 0)
                {
                    curUnit = pair.Key;
                    minTime = pair.Value;
                    if (pair.Key.Contains("SilverKnights"))
                    {
                        button.GetComponent<Image>().sprite = silverSprite;
                    }
                    if (pair.Key.Contains("KingdomKnights"))
                    {
                        button.GetComponent<Image>().sprite = kingdomSprite;
                    }
                }
                if(pair.Value <= 0)
                {
                    numberOfReady++;
                    numberReadyText.text = "" + numberOfReady;
                }
            }
            if(curUnit.Contains("SilverKnights"))
            {
                button.GetComponent<Image>().fillAmount = ((armyKD[curUnit] - armyCreating[curUnit]) / armyKD[curUnit]);
            }
            if(curUnit.Contains("KingdomKnights"))
            {
                button.GetComponent<Image>().fillAmount = (armyKD[curUnit] - armyCreating[curUnit]) / armyKD[curUnit];
            }
        }
    }

    public void UpdateKDtime()
    {
        armyCreating.Clear();
        //составление словаря
        for (int k = 0; k < valueList.Count; k++)
        {
            armyCreating.Add(keysList[k], valueList[k]);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventController : MonoBehaviour
{
    [SerializeField] private UIController UICtrl;
    [SerializeField] private PlayerInfo playerInfo;
    [SerializeField] private TutorialLists tutorialLists;
    private bool wasShowedWelcome = false;
    private bool wasShowedMainTask = false;
    private bool wasShowedRepAdvice = false;
    private bool wasShowedMoney = false;
    private bool wasShowRebelWarn = false;
    private bool wasShowRebelAttack = false;

    public void UpdateEvents()
    {
        TutorialEvents();
    }

    void TutorialEvents()
    {
        if (playerInfo.curMonth == "Фев" && playerInfo.year == 1200 && !wasShowedWelcome)
        {
            UICtrl.Tutorial();
            tutorialLists = FindObjectOfType<TutorialLists>();
            tutorialLists.WelcomeTutorial();
            wasShowedWelcome = true;
        }

        if (playerInfo.curMonth == "Март" && playerInfo.year == 1200 && !wasShowedMainTask)
        {
            UICtrl.Tutorial();
            tutorialLists = FindObjectOfType<TutorialLists>();
            tutorialLists.MainTask();
            wasShowedMainTask = true;
        }
        
        if (playerInfo.curMonth == "Май" && playerInfo.year == 1200 && !wasShowedRepAdvice)
        {
            UICtrl.Tutorial();
            tutorialLists = FindObjectOfType<TutorialLists>();
            tutorialLists.AdviceRep();
            wasShowedRepAdvice = true;
        }

        if (playerInfo.curMonth == "Июль" && playerInfo.year == 1200 && !wasShowedMoney)
        {
            UICtrl.Tutorial();
            tutorialLists = FindObjectOfType<TutorialLists>();
            tutorialLists.AdviceMoney();
            wasShowedMoney = true;
        }

        if (playerInfo.curMonth == "Авг" && playerInfo.year == 1201 && !wasShowRebelWarn)
        {
            UICtrl.Tutorial();
            tutorialLists = FindObjectOfType<TutorialLists>();
            tutorialLists.WarningRebel();
            wasShowRebelWarn = true;
        }

        if (playerInfo.curMonth == "Янв" && playerInfo.year == 1202 && !wasShowRebelAttack)
        {
            UICtrl.Tutorial();
            tutorialLists = FindObjectOfType<TutorialLists>();
            tutorialLists.RebelAttack();
            wasShowRebelAttack = true;
        }
    }
}

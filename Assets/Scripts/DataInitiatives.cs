﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class DataInitiatives
{
    public List<bool> values = new List<bool>();
    public List<string> keys = new List<string>();
}

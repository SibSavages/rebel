﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Darkening : MonoBehaviour
{
    [SerializeField] private float scaleDark;
    [SerializeField] private bool visible;
    private Image image;
    private Color color;
    public bool access = true;
    
    void Start()
    {
        image = GetComponent<Image>();
        color = image.color;
    }

    void Update()
    {
        if(access)
        {
            if (visible)
            {
                color.a -= Time.deltaTime * 0.5f * scaleDark;
                if (color.a <= 0)
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                color.a += Time.deltaTime * 0.5f * scaleDark;
                if (color.a >= 1)
                {
                    visible = true;
                    access = false;
                }
            }
            image.color = color;
        }
    }
}

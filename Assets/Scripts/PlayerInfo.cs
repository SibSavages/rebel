﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    [SerializeField] public Village[] village;
    public float coins;
    [SerializeField] public string[] months;
    public string curMonth;
    public float monthKD;
    public int numbMonth = 0;
    public int income;
    public int year;
    public float priceInitiatives;
    public float numberOfInflation = 0;
    public float maxinflation;
    public int numberInitiativesF;
    public int numberInitiativesO;
    public int numberInitiativesA;
    public float SupportF;
    public float SupportO;
    public float SupportA;
    public float maxSup;
    public int population;
    public float generalStability;
    public float reputation;


    void Start()
    {
        curMonth = months[0];
    }
}

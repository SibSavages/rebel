﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private UIController UICtrl;
    [SerializeField] private PlayerInfo playerInfo;
    [SerializeField] private GameObject enemyBase;
    public List<GameObject> bases = new List<GameObject>();
    public int numberVillage;
    private bool first = true;

    // Update is called once per frame
    public void EnemyCtrlUpdate()
    {
        FirstBase();
    }

    public void FirstBase()
    {
        if (playerInfo.curMonth == "Янв" && playerInfo.year == 1202 && first)
        {
            numberVillage = Random.Range(0, playerInfo.village.Length);
            Instantiate(enemyBase, playerInfo.village[numberVillage].transform.position + Vector3.right, Quaternion.Euler(90,0,0), playerInfo.village[numberVillage].transform);
            first = false;
        }
    }
}

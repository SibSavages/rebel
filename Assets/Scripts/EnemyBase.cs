﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    private GameObject newEnemy;
    public float timeSpawn;
    public float spawnKD;
    private Vector3 generatePos;


    private void Start()
    {
        generatePos = transform.position + Vector3.back*0.5f;
    }

    private void Update()
    {
        TimerKD();
    }

    private void TimerKD()
    {
        timeSpawn -= Time.deltaTime;
        if(timeSpawn<=0)
        {
            timeSpawn = spawnKD;
            GenerateEnemy();
        }
    }

    private void GenerateEnemy()
    {
        newEnemy = Instantiate(enemy, generatePos, Quaternion.identity);
        newEnemy.GetComponent<Enemy>().stayTargets.AddRange(GetComponentInParent<Village>().targets);
        newEnemy.GetComponent<Enemy>().curVillage = transform.parent.gameObject;
    }
}

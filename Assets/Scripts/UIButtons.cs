﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIButtons : MonoBehaviour
{
    public GameObject curWindow;

    public void Continue()
    {
        Time.timeScale = 1;
        Destroy(curWindow);
    }

    public void ToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
